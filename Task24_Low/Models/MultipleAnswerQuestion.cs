﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Low.Models
{
    public class MultipleAnswerQuestion : Question
    {
        public string[] AvailableAnswers { get; set; }
        public bool OneAnswer { get; set; }
    }
}