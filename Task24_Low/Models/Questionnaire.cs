﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Low.Models
{
    public class Questionnaire
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public Dictionary<int, string> Answers { get; set; }
    }
}