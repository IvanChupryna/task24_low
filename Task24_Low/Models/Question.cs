﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Low.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string QuestionText { get; set; }
    }
}