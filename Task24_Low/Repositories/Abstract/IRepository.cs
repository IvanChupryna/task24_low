﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task24_Low.Repositories.Abstract
{
    interface IRepository<TModel> where TModel : class
    {
        IEnumerable<TModel> GetAll();
        TModel GetById(int id);
        void Add(TModel model);
        void Delete(int id);
        void Update(TModel model);
    }
}
