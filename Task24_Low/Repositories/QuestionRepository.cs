﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Low.Models;
using Task24_Low.Repositories.Abstract;

namespace Task24_Low.Repositories
{
    public class QuestionRepository : IRepository<Question>
    {
        static readonly List<Question> _questions = new List<Question>()
        {
            new Question()
            {
                Id = 1,
                QuestionText = "What Food do you like?"
            },
            new Question()
            {
                Id = 2,
                QuestionText = "How quick is duck?"
            },
            new MultipleAnswerQuestion()
            {
                Id = 3,
                QuestionText = "What is Speed?",
                AvailableAnswers = new string[] {"McQueen", "Flesh", "QuickSilver"}
            },
            new MultipleAnswerQuestion()
            {
                Id = 4,
                QuestionText = "What is Speed?",
                AvailableAnswers = new string[] {"McQueen", "Flesh", "QuickSilver"}
            }
        };
        public void Add(Question model)
        {
            _questions.Add(model);
        }

        public void Delete(int id)
        {
            _questions.Remove(GetById(id));
        }

        public IEnumerable<Question> GetAll()
        {
            return _questions;
        }

        public Question GetById(int id)
        {
            return _questions.FirstOrDefault(q => q.Id == id);
        }

        public void Update(Question model)
        {
            Question questionToUpdate = GetById(model.Id);
            questionToUpdate.QuestionText = model.QuestionText;
        }
    }
}