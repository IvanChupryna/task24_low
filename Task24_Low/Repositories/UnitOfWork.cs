﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Low.Repositories
{
    public class UnitOfWork
    {
        private CommentRepository _commentRepository;
        private ArticleRepository _articleRepository;
        private QuestionRepository _questionRepository;
        private QuestionnaireRepository _questionnaireRepository;

        public CommentRepository commentRepository{
            get
            {
                if(_commentRepository == null)
                {
                    _commentRepository = new CommentRepository();
                }
                return _commentRepository;
            }
        }

        public ArticleRepository articleRepository
        {
            get
            {
                if(_articleRepository == null)
                {
                    _articleRepository = new ArticleRepository();
                }
                return _articleRepository;
            }
        }

        public QuestionRepository questionRepository
        {
            get
            {
                if(_questionRepository == null)
                {
                    _questionRepository = new QuestionRepository();
                }
                return _questionRepository;
            }
        }

        public QuestionnaireRepository questionnaireRepository
        {
            get
            {
                if (_questionnaireRepository == null)
                {
                    _questionnaireRepository = new QuestionnaireRepository();
                }
                return _questionnaireRepository;
            }
        }
    }
}