﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Low.Models;
using Task24_Low.Repositories.Abstract;

namespace Task24_Low.Repositories
{
    public class QuestionnaireRepository : IRepository<Questionnaire>
    {
        static readonly List<Questionnaire> _questionnaires = new List<Questionnaire>();
        public void Add(Questionnaire model)
        {
            _questionnaires.Add(model);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Questionnaire> GetAll()
        {
            throw new NotImplementedException();
        }

        public Questionnaire GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Questionnaire model)
        {
            throw new NotImplementedException();
        }
    }
}