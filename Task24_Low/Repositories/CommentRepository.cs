﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Low.Models;
using Task24_Low.Repositories.Abstract;

namespace Task24_Low.Repositories
{
    public class CommentRepository : IRepository<Comment>
    {
        static readonly List<Comment> _comments = new List<Comment>()
        {
            new Comment()
            {
                Id = 1,
                UserName = "Ivan",
                DateTimeCommented = new DateTime(2021, 8, 7, 23, 30, 0),
                Text = "Nice"
            }
        };

        public void Add(Comment model)
        {
            _comments.Add(model);
        }

        public void Delete(int id)
        {
            _comments.Remove(GetById(id));
        }

        public IEnumerable<Comment> GetAll()
        {
            return _comments;
        }

        public Comment GetById(int id)
        {
            return _comments.FirstOrDefault(c => c.Id == id);
        }

        public void Update(Comment model)
        {
            throw new NotImplementedException();
        }
    }
}