﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Low.Models;
using Task24_Low.Repositories;

namespace Task24_Low.Controllers
{ 
    public class CommentController : Controller
    {
        UnitOfWork unitOfWork;

        public CommentController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult ShowComments()
        {
            return View(unitOfWork.commentRepository.GetAll());
        }

        [HttpPost]
        public ActionResult ShowComments(FormCollection form)
        {
            if (form.Get("username") == String.Empty || form.Get("username").Contains(" "))
            {
                ModelState.AddModelError("username", "Please enter your username");
            }
            if(form.Get("comment_text") == String.Empty)
            {
                ModelState.AddModelError("comment", "Comment can't be empty");
            }

            if (ModelState.IsValid)
            {
                string name = form.Get("username");
                string comment_text = form.Get("comment_text");
                var currentComment = new Comment()
                {
                    Id = new Random().Next(),
                    UserName = name,
                    DateTimeCommented = DateTime.Now,
                    Text = comment_text
                };
                unitOfWork.commentRepository.Add(currentComment);
            }
      
            return View(unitOfWork.commentRepository.GetAll());
        }
    }
}