﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Low.Models;
using Task24_Low.Repositories;

namespace Task24_Low.Controllers
{
    public class ArticleController : Controller
    {
        UnitOfWork unitOfWork;

        public ArticleController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult ShowArticles()
        {
            var articles = unitOfWork.articleRepository.GetAll();
            return View(articles);
        }
    }
}