﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Low.Models;
using Task24_Low.Repositories;

namespace Task24_Low.Controllers
{
    public class QuestionnaireController : Controller
    {
        UnitOfWork unitOfWork;

        public QuestionnaireController()
        {
            unitOfWork = new UnitOfWork();
        }

        [HttpGet]
        public ActionResult ShowQuestions()
        {
            var questions = unitOfWork.questionRepository.GetAll();
            ViewBag.Questions = questions;
            return View();
        }

        [HttpPost]
        public ActionResult ShowResult(FormCollection form)
        {
            if(form["username"] == String.Empty)
            {
                ModelState.AddModelError("username", "Please enter username");
            }
            foreach (var key in form.AllKeys)
            {
                if(form[key] == String.Empty)
                {
                    ModelState.AddModelError(key, "Give valid answer");
                }
            }
            if (!form.AllKeys.Where(k => k.Contains("mult")).Any())
            {
                ModelState.AddModelError("mult", "Choose answer from checkboxes or radioboxes");
            }

            if (ModelState.IsValid)
            {
                Questionnaire questionnaire = new Questionnaire()
                {
                    Id = new Random().Next(),
                    UserName = form["username"]
                };

                Dictionary<int, string> answers = new Dictionary<int, string>();
                int counter = 1;
                foreach (var key in form.AllKeys)
                {
                    answers.Add(counter++, form[key]);
                }
                questionnaire.Answers = answers;
                unitOfWork.questionnaireRepository.Add(questionnaire);
                ViewBag.Answers = questionnaire;
            }

            return View();
        }
    }
}